log = require('grunt').log
_ = require('underscore')

module.exports = class Archive
  constructor: ({@htmlPath, @layout, @posts, @pageSize}) ->

  writeHtml: (generatesHtml, writesFile) ->
    if @htmlPath?
      posts = _(@posts).chain().sortBy((post) -> post.date).reverse().value()
      page = 1
      pages = Math.max(Math.ceil(@posts.length / @pageSize), 1)
      while page <= pages
        postsPage = _.first(posts, @pageSize)
        if page == 1
          htmlPath = "#{@htmlPath}index.html"
        else
          htmlPath = "#{@htmlPath}#{page}/index.html"

        writesFile.write generatesHtml.generate(@layout, undefined, { posts: postsPage, pages: pages, page: page }), htmlPath

        posts = _.rest(posts, @pageSize)
        page = page + 1
    else
      log.error "Archive not written because destination path is undefined"
