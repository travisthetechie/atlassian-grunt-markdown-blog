grunt = require('grunt')
pathlib = require('path')
Archive = require('../lib/archive')
Categories = require('../lib/categories')
Feed = require('../lib/feed')
GeneratesHtml = require('../lib/generates_html')
GeneratesRss = require('../lib/generates_rss')
Index = require('../lib/index')
Layout = require('../lib/layout')
Pages = require('../lib/pages')
Posts = require('../lib/posts')
Site = require('../lib/site')
WritesFile = require('../lib/writes_file')
Authors = require('../lib/authors')
DefaultContext = require('../lib/default_context')

_ = require('underscore')

module.exports = class MarkdownTask
  constructor: (@config) ->
    # default context templates, contains partials/etc
    defaultContext = new DefaultContext(@config.layouts.partials)

    @posts = new Posts @_allMarkdownPosts(),
      htmlDir: @config.dest
      layout: new Layout @config.layouts.post, defaultContext
      dateFormat: @config.dateFormat
      cwd: @config.paths.posts.cwd
      dest: @config.paths.posts.dest
      postFilter: @config.postFilter
    @categories = new Categories @posts,
      htmlDir: @config.pathRoots.categories
      layout: new Layout @config.layouts.category, defaultContext
      dateFormat: @config.dateFormat

    @authors = new Authors @posts,
      htmlDir: @config.pathRoots.authors
      layout: new Layout @config.layouts.author, defaultContext
      dateFormat: @config.dateFormat

    @pages = new Pages @_allMarkdownPages(),
      htmlDir: @config.dest
      layout: new Layout @config.layouts.page, defaultContext
      cwd: @config.paths.pages.cwd
      dest: @config.paths.pages.dest
    @index = new Index @posts.newest(),
      htmlPath: @config.paths.index
      layout: new Layout @config.layouts.index, defaultContext
    @archive = new Archive
      posts: @posts
      htmlPath: @config.paths.archive
      layout: new Layout @config.layouts.archive, defaultContext
      pageSize: @config.pageSize
    @feed = new Feed
      rssPath: @config.paths.rss
      postCount: @config.rssCount
    @site = new Site(@config, @posts, @pages)

  run: ->
    # Pages and posts already exist in the right location
    writesFile = new WritesFile(@config.dest)
    wrapper = new Layout(@config.layouts.wrapper, @config.context)
    generatesHtml = new GeneratesHtml(@site, wrapper)

    @posts.writeHtml generatesHtml, writesFile
    @pages.writeHtml generatesHtml, writesFile
    @index.writeHtml generatesHtml, writesFile
    @archive.writeHtml generatesHtml, writesFile
    @categories.writeHtml generatesHtml, writesFile
    @authors.writeHtml generatesHtml, writesFile

    @feed.writeRss new GeneratesRss(@site), writesFile

  #private
  _allMarkdownPosts: ->
    grunt.file.expand(@config.paths.posts, @config.paths.posts.src)

  _allMarkdownPages: ->
    grunt.file.expand(@config.paths.pages, @config.paths.pages.src)
