_ = require('underscore')
_.mixin(require('underscore.string').exports())
path = require("path")

module.exports = class Authors
  timeComparator = (post1, post2) ->
    post1.time().localeCompare(post2.time(), numeric: true)

  @:: = new Array
  constructor: (posts, {htmlDir, layout, dateFormat, comparator}) ->
    authors = []
    posts.sort(comparator || timeComparator)
    posts.forEach (post) ->
      author = post.attributes.author
      if (author and authors.indexOf(author) == -1)
        authors.push(author)

    authors.__proto__ = Authors::
    authors.layout = layout
    authors.htmlDir = htmlDir

    return authors

  writeHtml: (generatesHtml, writesFile) ->
    for author in @
      writesFile.write generatesHtml.generate(@layout, author), path.join(@htmlDir, author, "index.html")
