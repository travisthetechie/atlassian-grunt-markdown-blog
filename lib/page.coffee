_ = require('underscore')
grunt = require('grunt')
highlight = require('highlight.js')
marked = require('marked')
pathlib = require('./maybe_path_lib')
MarkdownSplitter = require('./markdown_splitter')

marked.setOptions
  highlight: (code, lang) ->
    highlighted = if highlight.LANGUAGES[lang]?
      highlight.highlight(lang, code, true)
    else
      highlight.highlightAuto(code)
    highlighted.value

module.exports = class Page
  constructor: (@htmlDir, @cwd, @path, @dest, @dateFormat) ->
    source = grunt.file.read(pathlib.join(@cwd, @path))
    splitted = new MarkdownSplitter().split(source)
    @markdown = splitted.markdown
    @attributes = splitted.header

  content: ->
    marked.parser(marked.lexer(@markdown))

  get: (name) ->
    attr = @attributes?[name]
    return unless attr?
    if _(attr).isFunction() then attr() else attr

  title: ->
    return @attributes?['title'] if @attributes?['title']?
    dasherized = @path.match(/\/\d{4}-\d{2}-\d{2}-([^/]*).md/)?[1]
    title = dasherized?.replace(/-/g, " ")
    title || @fileName()

  htmlPath: ->
    # the path as far as html is concerned
    # 1. Strip starting slash /
    # 2. Swap .md for .html
    # 3. Remove index.html if that's how it ends, we don't need that jazz
    htmlPath = @path.replace(/^\//, "").replace(/\.md$/, ".html").replace(/index\.html$/, "")

    if @dest?
      "#{@dest}/#{htmlPath}"
    else
      htmlPath

  diskPath: ->
    # the path where the file is on disk
    pathlib.join(@dest, @path.replace(/\.md$/, ".html"))

  fileName: ->
    name = @path.match(/\/([^/]*).md/)?[1]
    "#{name}.html"

  date: ->
    undefined
