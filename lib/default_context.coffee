_ = require('underscore')
grunt = require('grunt')
pathlib = require('path')

module.exports = class DefaultContext

  constructor: (partials) ->
    @templates = {}

    rawTemplateFiles = grunt.file.expand(partials)
    for rawTemplate in rawTemplateFiles
      key = pathlib.basename(rawTemplate).match(/^_(.*)\.us$/)[1]
      template = _.template(grunt.file.read(rawTemplate))

      @templates[key] = template

  partial: (partialName, context) ->
    template = @templates[partialName]
    return "" unless template?

    template(_.extend(@, context))