module.exports = class GeneratesHtml
  constructor: (@site, @wrapper) ->

  generate: (template, post, data = {}) ->
    context = data
    context.site = @site
    context.post = post

    context.yield = template.htmlFor(context)
    @wrapper.htmlFor(context)
