#global module:false

module.exports = (grunt) ->
  grunt.loadNpmTasks 'grunt-jasmine-bundle'
  grunt.loadNpmTasks 'grunt-release'
  grunt.loadNpmTasks 'grunt-contrib-watch'

  grunt.initConfig
    spec:
      unit:
        options:
          minijasminenode:
            isVerbose: false
            showColors: true
    watch:
      markdownSpec:
        files: [ "spec/**/*.{js,coffee}" ]
        tasks: [ "spec" ]

  grunt.registerTask 'default', ['spec']
